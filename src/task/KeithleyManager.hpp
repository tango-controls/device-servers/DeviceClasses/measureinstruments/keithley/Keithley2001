
//- Project : K20001 Multimeter
//- file : KeithleyManager.hpp
//- threaded reading of the HW


#ifndef __HP_MANAGER_H__
#define __HP_MANAGER_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include "Communication.hpp"


namespace Keithley2001_ns
{

//------------------------------------------------------------------------
//- KeithleyManager Class
//- read the HW 
//------------------------------------------------------------------------
class KeithleyManager : public yat4tango::DeviceTask
{
public :

  //- Constructeur/destructeur
  KeithleyManager ( Tango::DeviceImpl * host_device,
              std::string proxy_name,
              std::vector<std::string> config);

  virtual ~KeithleyManager ();

  //- save the configuration
  void save_configuration ();
  void restore_configuration ();


  //- returns the Keithley state and status
  Tango::DevState get_state_status (std::string& status);

  double get_data () {
    yat::AutoMutex<> guard(m_data_mutex);
    return m_output_value;
  }

  //- configure Keithley
  void configure ();

protected:
  //- process_message (implements yat4tango::DeviceTask pure virtual method)
  virtual void process_message (yat::Message& msg);

  //- mutex
  yat::Mutex m_data_mutex;
  
private :
  //- 
  std::string write_read (std::string);

  //- read back output value
  void read_value ();

  //- system error
  void get_error ();

  //- the host device 
  Tango::DeviceImpl * host_dev;

  //- communication device proxy name
  std::string m_proxy_name;

  //- config : cmds to send to the Keithley
  std::vector<std::string> m_vec_config;

  //- the communication link
  yat::SharedPtr<Communication> m_com;

  //- range
  double m_range;

  //- autorange?
  bool m_autorange_enabled;

  //- data
  double m_output_value;

  //- error(s)
  std::string m_error;
};

}//- namespace

#endif //- __HP_MANAGER_H__
