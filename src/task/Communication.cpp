// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.cpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#include "Communication.hpp"

// ----------------------------------------------------------------------------
//- commands
static const std::string WRITE_CMD          = "Write";
static const std::string READ_CMD           = "Read";
static const std::string WRITE_READ_CMD     = "WriteRead";

namespace Keithley2001_ns
{
// ============================================================================
// Communication::Communication
// ============================================================================
Communication::Communication (Tango::DeviceImpl * host_device,
                              std::string comDevName)
: yat4tango::TangoLogAdapter(host_device),
  m_dsproxy (0),
  m_host_dev(host_device),
  m_dev_name(comDevName)
{

}

// ============================================================================
// Communication::~Communication
// ============================================================================
Communication::~Communication (void)
{
  delete_proxy();
}

// ============================================================================
// Communication::create_proxy
// ============================================================================
void Communication::create_proxy()
{
  if ( !m_dev_name.empty() )
  {
    try
    {
      //- create proxy
      m_dsproxy.reset(new Tango::DeviceProxyHelper(m_dev_name, m_host_dev));
      //- 
      m_dsproxy->get_device_proxy()->ping();
    }
    catch(Tango::DevFailed& df)
    {
      FATAL_STREAM << df << std::endl;
      delete_proxy();
      throw df;
    }
    catch(...)
    {
      FATAL_STREAM << "Communication::Failed to create proxy on \"" << m_dev_name << "\" : (received [...] exception)" << std::endl;
      delete_proxy();
      throw;
    }
  }
}

// ============================================================================
// Communication::delete_proxy
// ============================================================================
void Communication::delete_proxy()
{
  m_dsproxy.reset();
}

// ============================================================================
// Communication::check_proxy
// ============================================================================
void Communication::check_proxy()
{
  if ( !m_dsproxy )
  {
    Tango::Except::throw_exception("INITIALIZATION_ERROR",
                                   "Communication link proxy is not created.",
                                   "Communication::check_proxy");
  }
  else
  {
    try
    {
      //- check device is up!
      m_dsproxy->get_device_proxy()->ping();
    }
    catch(Tango::DevFailed& df)
    {
      ERROR_STREAM << df << std::endl;
      throw df;
    }
  } 
}

// ============================================================================
// Communication::get_proxy_state
// ============================================================================
Tango::DevState Communication::get_proxy_state()
{
  Tango::DevState state = Tango::UNKNOWN;
  try
  {
    check_proxy();

    //- read back com link state
    state = m_dsproxy->get_device_proxy()->state();
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << df << std::endl;
    throw df;
  }

  return state;
}

// ============================================================================
// Communication::write
// ============================================================================
void Communication::write(std::string cmd_to_send)
{
  try
  {
    DEBUG_STREAM << "Communication::write -> for cmd [" << cmd_to_send << "]"<< std::endl;
    check_proxy();

    m_dsproxy->command_in(WRITE_CMD, cmd_to_send);
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << df << std::endl;
    throw df;
  }
}

// ============================================================================
// Communication::read
// ============================================================================
std::string Communication::read()
{
  std::string response("");
  
  try
  {
    check_proxy();

    m_dsproxy->command_out(READ_CMD, response);
    DEBUG_STREAM << "Communication::read -> response [" << response << "]" << std::endl;
  }
  catch(Tango::DevFailed& df)
  {
    ERROR_STREAM << df << std::endl;
    throw df;
  }

  return response;
}

// ============================================================================
// Communication::write_read
// ============================================================================
std::string Communication::write_read(std::string cmd)
{
  static std::string empty("");
  std::string response("");

  try
  {
    DEBUG_STREAM << "Communication::WRITE_READ -> for cmd [" << cmd << "]" << std::endl;
    
    check_proxy();

    //- send the cmd 
    m_dsproxy->command_inout(WRITE_READ_CMD, cmd, response);
    
    DEBUG_STREAM << "Communication::WRITE_READ -> resp [" << response << "]" << std::endl;
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << df << std::endl;
    throw df;
  }
  catch(...)
  {
    ERROR_STREAM << "Communication::write_read received a [...] exception." << std::endl;
    throw;
  }
  
  return response;
}

} //- end namespace
