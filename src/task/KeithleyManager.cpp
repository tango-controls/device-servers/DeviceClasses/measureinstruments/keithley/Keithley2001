
//- Project : Keithley2001
//- file : KeithleyManager.cpp

#include "task/KeithleyManager.hpp"
#include <iomanip>
#include <yat/utils/XString.h>  //- to_num
#include <yat/utils/String.h>   //- to_upper
#include <yat/Portability.h>    //- NaN

// ========================================================
const std::size_t PERIODIC_MSG_PERIOD    = 5000;            //- in ms
const std::size_t MESSAGE_TIMEOUT        = 2500;
//---------------------------------------------------------
//- the YAT user messages
const std::size_t EXEC_LOW_LEVEL_MSG     = yat::FIRST_USER_MSG + 1000;
const std::size_t SAVE_CONFIG_MSG        = yat::FIRST_USER_MSG + 1010;
const std::size_t RESTORE_CONFIG_MSG     = yat::FIRST_USER_MSG + 1011;
//---------------------------------------------------------
const std::string COMMENT                = "#";
const std::string END_OF_LINE            = "\n";
//---------------------------------------------------------
//- commands
const std::string CMD_SAVE_CONFIG        = "*SAV ";
const std::string CMD_RESTORE_CONFIG     = "*RCL ";
const std::string CMD_READ_ERROR         = "SYST:ERR?";
const std::string CMD_READ_LAST_VALUE    = "READ?";
const std::string NO_ERROR               = "No error";
//---------------------------------------------------------
const long SLEEP_IN_SECONDS              = 0;
const long SLEEP_BETWEEN_TWO_ACTIONS     = 50000000;        //- 50 milli seconds
//---------------------------------------------------------
const std::size_t MIN_SLOT_COUNT         = 0;
const std::size_t MAX_SLOT_COUNT         = 4;


namespace Keithley2001_ns
{
//-----------------------------------------------
//- Ctor ----------------------------------------
KeithleyManager::KeithleyManager (Tango::DeviceImpl * host_device, 
                      std::string proxy_name,
                      std::vector<std::string> config)
: yat4tango::DeviceTask(host_device),
  host_dev (host_device),
  m_proxy_name(proxy_name),
  m_vec_config(config),
  m_com (0),
  m_output_value(yat::IEEE_NAN),
  m_error(NO_ERROR)
{
  DEBUG_STREAM << "KeithleyManager::KeithleyManager <- " << std::endl;
}


//-----------------------------------------------
//- Dtor ----------------------------------------
KeithleyManager::~KeithleyManager (void)
{
  DEBUG_STREAM << "KeithleyManager::~KeithleyManager <- " << std::endl;
  //- Noop
}

//-----------------------------------------------
//- the user core of the Task -------------------
void KeithleyManager::process_message (yat::Message& _msg)
{
  DEBUG_STREAM << "KeithleyManager::handle_message::receiving msg " << _msg.to_string() << std::endl;

  //- handle msg
  switch (_msg.type())
  {
    //- THREAD_INIT =======================
  case yat::TASK_INIT:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message::THREAD_INIT::thread is starting up" << std::endl;
      //- "initialization" code goes here
      try
      {
        m_com.reset(new Communication(host_dev, m_proxy_name));

        m_com->create_proxy();

        //- halt a measurement in progress and place the multimeter in the “idle state.”
        // stop_integration();

        //- send default configuration
        configure();

        //- empty error buffer(if any)
        std::string resp("");
        do
        {
          resp = write_read(CMD_READ_ERROR);
        }
        while(resp.find(NO_ERROR) == std::string::npos);

        //- yat::Task configure optional msg handling
        enable_timeout_msg(false);
        enable_periodic_msg(true);
        set_periodic_msg_period(PERIODIC_MSG_PERIOD);
      }
      catch (...)
      {
        ERROR_STREAM << "KeithleyManager::init_device()*** yat::SocketException caught ***" << std::endl;
        m_com.reset();
      } 
    } 
    break;
    //- TASK_EXIT =======================
  case yat::TASK_EXIT:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling TASK_EXIT thread is quitting" << std::endl;
      
      // stop_integration();

      //- "release" code goes here
      m_com.reset();
    }
    break;
    //- TASK_PERIODIC ===================
  case yat::TASK_PERIODIC:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling TASK_PERIODIC msg " << std::endl;
      //- code relative to the task's periodic job goes here
      try
      {
        read_value();

        get_error();
      }
      catch(...)
      {
        //- CAREFULL:
        //- if NPLC is set to 100, a (CORBA) timeout exception is sent but the Keithley
        //- put the integrated value in its output buffer. Prior to any other command
        //- it is mandatory to read back the output buffer!!!
        //- A sleep of 2 seconds is the minimum to get the integrated value.
        //- This is the reason the exception is not managed here and why a Read command is sent.
        INFO_STREAM << "\n\n\tPERIODIC caugth [....] ; trying READ ..." << std::endl;
        try
        {
          yat::ThreadingUtilities::sleep(2,0);
          //- direct access to HW
          std::string resp = m_com->read();
          {//- critical section
            yat::AutoMutex<> guard(m_data_mutex);

            m_output_value = yat::XString<Tango::DevDouble>::to_num (resp);
          }
          INFO_STREAM << "\t... received [" << resp << "]\n" << std::endl;
        }
        catch(...)
        {
          ERROR_STREAM << "\t ERROR -> PERIODIC caugth [....] ; sending READ ..." << std::endl;
          m_output_value = yat::IEEE_NAN;
        }
      }

    }
    break;
    //- TASK_TIMEOUT ===================
  case yat::TASK_TIMEOUT:
    {
      //- code relative to the task's tmo handling goes here
      ERROR_STREAM << "KeithleyManager::handle_message handling TASK_TIMEOUT msg" << std::endl;
    }
    break;
  //- USER_DEFINED_MSG ================
  //- low level commands (ASCII commands directly passed to the Lakeshore 336 (appending LF at the end)
  case EXEC_LOW_LEVEL_MSG:
    {
      DEBUG_STREAM << "KeithleyManager::handle_message handling EXEC_LOW_LEVEL_MSG msg" << std::endl;
      // LowLevelMsg * llm = 0;
      // _msg.detach_data(llm);
      // if (llm)
      // {
      //   this->exec_low_level_command (llm->cmd);
      //   delete llm;
      // }
    }
    break;
  case SAVE_CONFIG_MSG:
      {
        DEBUG_STREAM << "KeithleyManager::handle_message handling SAVE_CONFIG_MSG msg" << std::endl;
        Tango::DevUShort* slot_value = 0;
        _msg.detach_data(slot_value);
        //- prepare command
        std::stringstream s;
        s << CMD_SAVE_CONFIG 
          << *slot_value 
          << std::ends;
        //- send
        write_read (s.str ());
      }
      break;
  case RESTORE_CONFIG_MSG:
      {
        DEBUG_STREAM << "KeithleyManager::handle_message handling RESTORE_CONFIG_MSG msg" << std::endl;

        Tango::DevUShort* slot_value = 0;
        _msg.detach_data(slot_value);
        //- prepare command
        std::stringstream s;
        s << CMD_RESTORE_CONFIG 
          << *slot_value 
          << std::ends;
      }
      break;
  default:
		  ERROR_STREAM<< "KeithleyManager::handle_message::unhandled msg type received" << std::endl;
		break;
  } //- switch (_msg.type())
} //- KeithleyManager::process_message

//-----------------------------------------------
//- configure Keithley for acquisition
//-----------------------------------------------
void KeithleyManager::configure()
{
  std::size_t nb_cmd = m_vec_config.size();
  for (std::size_t idx = 0; idx < nb_cmd; idx++)
  {
    //- commands beginning with # are comments
    if (m_vec_config.at(idx).find(COMMENT) != std::string::npos)
    {
      //- skip it
      continue;
    }
    else
    {
      write_read(m_vec_config.at(idx));
    }
  }
}

//-----------------------------------------------
//- save_configuration
//-----------------------------------------------
void KeithleyManager::save_configuration()
{
  Tango::DevUShort slot_num = 0;
  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(SAVE_CONFIG_MSG);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::save_configuration error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::save_configuration");
  }

  msg->attach_data (slot_num);
  
  post(msg);
}

//-----------------------------------------------
//- restore_configuration
//-----------------------------------------------
void KeithleyManager::restore_configuration()
{
  bool wait = true;
  Tango::DevUShort slot_num = 0;

  //- prepare command
  yat::Message * msg = 0;
  msg = new yat::Message(RESTORE_CONFIG_MSG, MAX_USER_PRIORITY, wait);
  if ( !msg )
  {
    ERROR_STREAM << "KeithleyManager::restore_configuration error trying to create msg "<< endl;
    Tango::Except::throw_exception ("OUT_OF_MEMORY",
                                    "yat::Message allocation failed",
                                    "KeithleyManager::restore_configuration");
  }
  
  msg->attach_data (slot_num);

  post(msg);
}

//-----------------------------------------------
//- returns state/status
//-----------------------------------------------
Tango::DevState KeithleyManager::get_state_status (std::string& status_)
{
  DEBUG_STREAM << "KeithleyManager::get_state_status <-" << std::endl;
  Tango::DevState argout = Tango::UNKNOWN;
  //- error code separator
  std::string separator(",");
  std::string status("");
 //- case of  device proxy error
  if ( !m_com )
  {
    argout  = Tango::FAULT;
    status_ = "Failed to create proxy!";
  }
  else
  {
    {//- critical section
      yat::AutoMutex<> guard(m_data_mutex);

      status = m_error;
    }
    if (status.find(NO_ERROR) == std::string::npos)
    {
      argout = Tango::ALARM;
    }
    else
    {
      //- erase error code
      // std::size_t it = status.find(separator);
      // if (it != std::string::npos)
      // {
      //   status.erase(0,it+1);
      // }
      argout = Tango::ON;
    }
    status_ = status;
  }

  return argout;
 }

//-----------------------------------------------
//- read_value
//-----------------------------------------------
void KeithleyManager::read_value ()
{
  std::string response("");
  static std::string cmd(CMD_READ_LAST_VALUE);

  response = write_read(cmd);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_output_value = yat::XString<Tango::DevDouble>::to_num (response);
  }
} 

//-----------------------------------------------
//- get_error
//-----------------------------------------------
void KeithleyManager::get_error ()
{
  std::string response("");

  response = write_read(CMD_READ_ERROR);

  {//- critical section
    yat::AutoMutex<> guard(m_data_mutex);

    m_error = response;
  }
}

//-----------------------------------------------
//- write_read
//- send command returns result
//-----------------------------------------------
std::string KeithleyManager::write_read (std::string cmd)
{
  INFO_STREAM << "KeithleyManager::write_read <- for cmd [" << cmd << "]" << std::endl;
  std::string empty("");
  std::string response("");

  //- case of  device proxy error
  if ( !m_com )
  {
    return empty;
  }

  try
  {
    //- command need a response?
    if ( cmd.rfind("?") != std::string::npos )
    {
      response = m_com->write_read(cmd);
      //- erase caractere
      response.erase(response.find(END_OF_LINE));
      INFO_STREAM << "KeithleyManager::write_read() cmd [" << cmd << "] response [" << response << "]." << std::endl;
    }
    else
    {
      //- only write command
      m_com->write(cmd);
      INFO_STREAM << "KeithleyManager::write_read() cmd [" << cmd << "] (no response)." << std::endl;
    }
    //- Keithley needs 20milliseconds between two commands
    yat::ThreadingUtilities::sleep(SLEEP_IN_SECONDS, SLEEP_BETWEEN_TWO_ACTIONS);

  }
  catch (Tango::DevFailed&  df)
  {
    ERROR_STREAM << "KeithleyManager::write_read() -> caugth DevFailed for command [" << cmd << "]." << std::endl;
    ERROR_STREAM << df << std::endl;
    return empty;
  } 
  catch (...)
  {
    ERROR_STREAM << "KeithleyManager::write_read() -> caugth [...] for command [" << cmd << "]." << std::endl;
    return empty;
  }

  return response;
}

} //- namespace
