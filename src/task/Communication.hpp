// ============================================================================
//
// = CONTEXT
//    TANGO Project - Communication Class
//
// = FILENAME
//    Communication.hpp
//
// = AUTHOR
//    X. Elattaoui
// ============================================================================

#ifndef _COM_CLASS_H_
#define _COM_CLASS_H_

#include <tango.h>
#include <string>
#include <DeviceProxyHelper.h>
#include <yat4tango/LogHelper.h>
#include <yat/memory/SharedPtr.h>

/**
 *  \brief Class to manage communication link
 *
 *  \author Xavier Elattaoui
 *  \date 03-2020
 */
namespace Keithley2001_ns
{

class Communication : public yat4tango::TangoLogAdapter
{
public :
  
  /**
  *  \brief Initialization.
  */
  Communication (Tango::DeviceImpl * host_device, std::string comDevName);

  /**
  *  \brief Release resources.
  */
  virtual ~Communication ();
  
  void create_proxy();

  /**
  *  \brief Sends command and returns the response.
  */
  std::string write_read(std::string);
  
  /**
  *  \brief Sends command
  */
  void write(std::string);
  
  /**
  *  \brief Read back response
  */
  std::string read();
  
  /**
  *  \brief Returns proxy state
  */
  Tango::DevState get_proxy_state();
  
private :
  
  void delete_proxy();
  
  void check_proxy();
  
  //- proxy
  yat::SharedPtr<Tango::DeviceProxyHelper> m_dsproxy;

  //- the host device
  Tango::DeviceImpl*        m_host_dev;
  
  //- communication device name
  std::string               m_dev_name;

};

} //- end namespace

#endif // _COM_CLASS_H_
